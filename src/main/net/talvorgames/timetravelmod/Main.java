package main.net.talvorgames.timetravelmod;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class Main implements ModInitializer {

	// public static final FabricItem IDK_YET = new FabricItem(new
	// FabricItemSettings().group(ItemGroup.MISC));

	public static final FabricBlock SELF_REPAIRING_CONCRETE = new FabricBlock(
			FabricBlockSettings.of(Material.METAL).hardness(20.0f));

	@Override
	public void onInitialize() {
		// This code runs as soon as Minecraft is in a mod-load-ready state.
		// However, some things (like resources) may still be uninitialized.
		// Proceed with mild caution.

		System.out.println("Hello Fabric world!");

		Registry.register(Registry.BLOCK, new Identifier("timetravelmod", "self_repairing_concrete"),
				SELF_REPAIRING_CONCRETE);

		Registry.register(Registry.ITEM, new Identifier("timetravelmod", "self_repairing_concrete"),
				new BlockItem(SELF_REPAIRING_CONCRETE, new FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS)));

		// Registry.register(Registry.ITEM, new Identifier("timetravelmod",
		// "self_repairing_concrete"),
		// SELF_REPAIRING_CONCRETE);
	}

}
